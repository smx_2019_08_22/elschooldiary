package Business.SessionBeans;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import Entities.Korisnici;

/**
 *
 * @author Igor Strbac <mail@igorstrbac.com>
 */
@javax.ejb.Stateless
public class LoginBean implements LoginBeanLocal {

    @PersistenceContext(unitName = "SchoolDiaryPU"    )
    private EntityManager em;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public Korisnici login(String username, String password) {

        try {
            Query query = em.createNamedQuery("Korisnik.findByKorisnickoImeLozinka");
            query.setParameter("korisnickoIme", username);
            query.setParameter("lozinka", password);

            Korisnici result = (Korisnici) query.getSingleResult();

            return result;
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

}
