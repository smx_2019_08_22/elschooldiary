package Business.SessionBeans;

import javax.ejb.Local;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import Entities.Korisnici;

/**
 *
 * @author Igor Strbac <mail@igorstrbac.com>
 */
@Local
public interface LoginBeanLocal
{
        @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Korisnici login(final String username, final String password);

}
