package UserController;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class UserController {
    
    private String ulogovaniKorisnik;
    
    public void ispis(){
        System.out.println("Ulogovani korisnik:"+ulogovaniKorisnik);
    }
    public String getUlogovaniKorisnik() {
        return ulogovaniKorisnik;
    }

    public void setUlogovaniKorisnik(String ulogovaniKorisnik) {
        this.ulogovaniKorisnik = ulogovaniKorisnik;
    }
    
    
}
