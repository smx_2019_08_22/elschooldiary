package ManagedBeans;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import Business.SessionBeans.LoginBeanLocal;
import Entities.Korisnici;

/**
 *
 * @author Igor Strbac <mail@igorstrbac.com>
 */
@ManagedBean(name = "loginMB", eager = true)
@SessionScoped
public class LoginManagedBean implements Serializable {

    private String username;
    private String password;

    private String message = "";

    private Korisnici loggedInUser = null;

    @EJB
    private LoginBeanLocal loginBean;

    public String login() {

        if (loggedInUser != null) {
            return "success";
        }

        Korisnici korisnici = loginBean.login(username, password);

        if (korisnici == null) {
            message = "Neispravna kombinacija korisničkog imena i lozinke.";
            return "failure";
        }

        this.loggedInUser = korisnici;

        return "success";
    }

    public void logout() {
        loggedInUser = null;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Korisnici getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(Korisnici loggedInUser) {
        this.loggedInUser = loggedInUser;
    }
    
    public boolean isLoggedIn() {
        return (loggedInUser != null);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
